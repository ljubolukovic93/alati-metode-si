(ns projectapp.mysql
  (:require [clojure.java.jdbc :as sql]))

(def db-spec {
     :subprotocol "mysql"
     :subname "//localhost:3306/projectapp"
     :user "root"
     :password ""
     :useSSL false
     :autoReconnect true})

(defn parse-int [s]
   (Integer. (re-find  #"\d+" s )))

(defn list-projects [orderby]
(if orderby 
  (sql/query db-spec [(str "SELECT * FROM project ORDER BY " orderby)])
       (sql/query db-spec ["SELECT * FROM project" ]))
)

(defn list-projects-paging [page size orderby]
   (if (and page size) 
      (sql/query db-spec [(str "SELECT * FROM project ORDER BY "
                               (if orderby orderby
                                 "id ASC")
       " LIMIT " (* (parse-int page) ( parse-int size)) ", " size)])
      (sql/query db-spec ["SELECT * FROM project" ])
      ))

(defn list-projects-query [query]
(if query 
  (sql/query db-spec [(str "SELECT * FROM project WHERE TITLE LIKE '%" query "%' OR DESCRIPTION LIKE '%" query "%'")])
       (sql/query db-spec ["SELECT * FROM project" ]))
)

(defn single-project [id] 
  (sql/query db-spec ["SELECT * FROM project WHERE id=?" id] ))

(defn count-projects [] 
  (sql/query db-spec ["COUNT(*) FROM project"]
                 {:result-set-fn first}))

(defn delete-project [id] 
  (sql/delete! db-spec :project ["id = ?" id]))

(defn insert-project [project] 
  (sql/insert! db-spec :project project))

(defn update-project [project] 
  (sql/update! db-spec :project project ["id = ?" (get project :id)]))
