(ns projectapp.handler
  (:use compojure.core)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.middleware.json :as middleware]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.util.response :refer [response]]
            [projectapp.mysql :as mysql]))

(defroutes app-routes
  (route/resources "/")
  (GET "/" [] "Hello World")
  
  (GET "/projects" [orderby]  (mysql/list-projects orderby))
  (GET "/projects/paging" [page size orderby]  (mysql/list-projects-paging page size orderby))
  (GET "/projects/filter" [query]  (mysql/list-projects-query query))
  (GET "/projects/:id" [id] (mysql/single-project id))
  (POST "/projects" {params :params} (mysql/insert-project params))
  (PUT "/projects" {params :params} (mysql/update-project params))
  (DELETE "/projects/:id" [id] (mysql/delete-project id))
  (route/not-found (response {:msg "Not Found", :code 404})))


(defn wrap-content-json [h]
  (fn [req] (assoc-in (h req) [:headers "Content-Type"] "application/json; charset=utf-8")))

(def app
  (-> (handler/api app-routes)
      (middleware/wrap-json-body)
      (middleware/wrap-json-params)
      (middleware/wrap-json-response)
      (wrap-cors :access-control-allow-origin  [#"http://localhost:4200"]
                 :access-control-allow-methods [:get :put :post :delete :options])
      (wrap-content-json)))
