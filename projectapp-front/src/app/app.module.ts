import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ProjectService } from './services/project.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatIconModule, MatTableModule, MatToolbarModule,
  MatFormFieldModule, MatPaginatorModule, MatSortModule, MatDialogModule,
  MatInputModule, MatSnackBarModule, MatDatepickerModule, MatNativeDateModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { DeleteDialogComponent } from './components/delete/delete.dialog.component';
import { AddDialogComponent } from './components/add/add.dialog.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule, MatIconModule,
    MatTableModule, MatPaginatorModule, MatSortModule,
    MatToolbarModule, MatFormFieldModule, MatDialogModule,
    MatInputModule, MatSnackBarModule, MatDatepickerModule, MatNativeDateModule, ReactiveFormsModule
  ],
  declarations: [
    AppComponent, AddDialogComponent, DeleteDialogComponent
  ],
  entryComponents: [
    AddDialogComponent,
    DeleteDialogComponent
  ],
  providers: [HttpClient, ProjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
