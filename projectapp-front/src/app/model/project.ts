
export class Project {

    id: number;
    title: string;
    description?: string;
    start_date?: any;
    end_date?: any;

}
