import { Project } from './model/project';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { DataSource } from '@angular/cdk/table';

export class ProjectDataSource extends DataSource<any> {

    constructor(public projects: Project[]) {
        super();
    }

    connect(): Observable<Project[]> {
        return Observable.of(this.projects);
    }

    disconnect() { }

}
