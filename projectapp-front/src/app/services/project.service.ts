import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/timeout';
import { Project } from '../model/project';

@Injectable()
export class ProjectService {

  appUrl = 'http://localhost:3000/';
  uri = 'projects';

  constructor(private http: HttpClient) {
  }

  protected getOptions(params1?: HttpParams) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json');
    headers = headers.set('Accept', 'application/json');
    headers = headers.set('Access-Control-Allow-Origin', '*');
    if (params1) {
      return {
        params: params1,
        headers: headers
      };
    }
    return {
      headers: headers
    };
  }

  getAllPaging(page?: number, size?: number) {
    let params: HttpParams = new HttpParams();
    if (page) {
      params = params.set('page', page.toString());
    }

    params = params.set('orderby', 'id+asc');

    if (size) {
      params = params.set('size', size.toString());
    }
    return this.http.get<Project[]>(this.appUrl + this.uri + '/paging', this.getOptions(params)).timeout(10000);
  }

  getAllFilter(query: string) {
    let params: HttpParams = new HttpParams();
    params = params.set('query', query);
    return this.http.get<Project[]>(this.appUrl + this.uri + '/filter', this.getOptions(params)).timeout(10000);
  }

  getAll() {
    return this.http.get<Project[]>(this.appUrl + this.uri).timeout(10000);
  }

  getOne(id: number) {
    return this.http.get<Project>(this.appUrl + this.uri + '/' + id, this.getOptions()).timeout(10000);
  }

  create(entity: Project) {
    entity = this.replaceDates(entity);
    return this.http.post<any>(this.appUrl + this.uri, entity, this.getOptions()).timeout(10000);
  }

  update(entity: Project) {
    entity = this.replaceDates(entity);
    return this.http.put<any>(this.appUrl + this.uri, entity, this.getOptions()).timeout(10000);
  }

  delete(id: number) {
    return this.http.delete(this.appUrl + this.uri + '/' + id, this.getOptions()).timeout(10000);
  }

  replaceDates(entity: Project) {
    const project = { ...entity };
    if (project) {
      if (entity.start_date) {
        project.start_date =
          entity.start_date.getUTCFullYear() + '-' + (entity.start_date.getMonth() + 1) + '-' + entity.start_date.getDate();
      }
      if (entity.end_date) {
        project.end_date =
          entity.end_date.getUTCFullYear() + '-' + (entity.end_date.getMonth() + 1) + '-' + entity.end_date.getDate();
      }
    }
    return project;
  }

}
