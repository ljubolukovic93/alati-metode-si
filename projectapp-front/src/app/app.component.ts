import { ProjectService } from './services/project.service';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, PageEvent, MatSnackBar, Sort } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Project } from './model/project';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AddDialogComponent } from './components/add/add.dialog.component';
import { DeleteDialogComponent } from './components/delete/delete.dialog.component';
import { ProjectDataSource } from './project-data-source';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  displayedColumns = ['id', 'title', 'start_date', 'end_date', 'actions'];

  dataSource: ProjectDataSource | null;
  id: number;
  projects: Project[];

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions = [5, 10, 20];
  pageIndex = 0;

  @ViewChild('filter') filter: ElementRef;

  constructor(public dialog: MatDialog,
    private projectService: ProjectService, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.refresh();

    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        this.refresh();
      });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 4000,
    });
  }

  refresh() {
    this.projectService.getAllFilter(this.filter.nativeElement.value).subscribe(
      projects => {
        this.projects = projects;
        this.length = projects.length;
        this.constructDataSource();
      },
      err => this.openSnackBar('An error has occurred during loading of projects.')
    );
  }

  addOrEditProject(project: Project) {
    this.id = project.id;
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: {...project}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  deleteItem(project: Project) {
    this.id = project.id;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: project
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.projectService.delete(this.id).subscribe(
          data => {
            this.openSnackBar('Project successfuly deleted.');
            this.refresh();
          },
          err => this.openSnackBar('Project cannot be deleted.')
        );
      }
    });
  }

  onPageEvent(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.constructDataSource();
  }

  sortData(sort: Sort) {
    if (!sort.active || sort.direction == '') {
      return;
    }
    this.projects = this.projects.sort((a, b) => {
      const isAsc = sort.direction == 'asc';
      switch (sort.active) {
        case 'id': return compare(a.id, b.id, isAsc);
        case 'title': return compare(a.title, b.title, isAsc);
        default: return 0;
      }
    });
    this.constructDataSource();
  }

  constructDataSource() {
    if (this.projects) {
      const paginatedArray = this.projects.slice(this.pageIndex * this.pageSize, (this.pageIndex + 1) * this.pageSize);
      this.dataSource = new ProjectDataSource(paginatedArray);
    }
  }

}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
