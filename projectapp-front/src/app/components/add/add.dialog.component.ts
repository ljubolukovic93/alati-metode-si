import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Component, Inject, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { FormControl, Validators } from '@angular/forms';
import { Project } from '../../model/project';

@Component({
  selector: 'app-add.dialog',
  templateUrl: './add.dialog.component.html',
  styleUrls: ['./add.dialog.component.css']
})
export class AddDialogComponent implements OnInit {

  title = 'Add new Project';
  projectId = null;
  startDate: FormControl;
  endDate: FormControl;

  formControl = new FormControl('', [
    Validators.required
  ]);

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Project,
    public projectService: ProjectService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    if (this.data.start_date) {
      const stringDate: any = this.data.start_date;
      this.startDate = new FormControl(new Date(stringDate));
    } else {
      this.startDate = new FormControl(null);
    }
    if (this.data.end_date) {
      const stringDate: any = this.data.end_date;
      this.endDate = new FormControl(new Date(stringDate));
    } else {
      this.endDate = new FormControl(null);
    }
    if (this.data && this.data.id) {
      this.projectId = this.data.id;
      this.title = 'Edit Project';
    }
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirm(): void {
    if (this.startDate) {
      this.data.start_date = this.startDate.value;
    }
    if (this.endDate) {
      this.data.end_date = this.endDate.value;
    }
    if (this.projectId) {
      this.data.id = this.projectId;
      this.projectService.update(this.data).subscribe(
        data => this.openSnackBar('Successfully updated'),
        err => this.openSnackBar('Error happened ' + err));
    } else {
      this.projectService.create(this.data).subscribe(
        data => this.openSnackBar('Successfully created'),
        err => this.openSnackBar('Error happened ' + err));
    }
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 4000,
    });
  }

}
