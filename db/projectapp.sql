/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.7.14 : Database - projectapp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`projectapp` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `projectapp`;

/*Table structure for table `project` */

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE latin2_croatian_ci NOT NULL,
  `description` varchar(300) COLLATE latin2_croatian_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;

/*Data for the table `project` */

insert  into `project`(`id`,`title`,`description`,`start_date`,`end_date`,`status`) values 
(1,'Football tournament','Local amateur football tournament. Knockout system, 8 teams.','2018-09-21','2018-09-23',NULL),
(2,'Develop website','Complete development of client website.','2018-03-03','2018-04-04',NULL),
(3,'Paint house','Paint house with father over next weekend.','2018-04-05',NULL,NULL),
(6,'University Project','Complete paper for my next exam.','2018-04-01','2018-06-04',NULL),
(7,'New Project','my new',NULL,NULL,NULL),
(8,'Trip to Lisabon','Plan, organize and visit Lisabonthis autumn.','2018-09-01',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
